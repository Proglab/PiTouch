#!/usr/bin/python

import RPi.GPIO as GPIO
import time

relay1 = 2
relay2 = 3
relay3 = 14
relay4 = 17
relay5 = 27
relay6 = 22
relay7 = 10
relay8 = 9

button1 = 21


GPIO.setmode(GPIO.BCM)

# init list with pin numbers

pinList = [relay8]

# loop through pins and set mode and state to 'low'

GPIO.setup(button1, GPIO.IN, pull_up_down=GPIO.PUD_UP)


for i in pinList:
    GPIO.setup(i, GPIO.OUT)
    GPIO.output(i, GPIO.HIGH)

SleepTimeL = 1

# main loop

i = 0
while i == 0:
    if GPIO.input(button1) == 0:
        print("Button Pressed")
        if GPIO.input(relay8) == GPIO.HIGH:
            GPIO.output(relay8, GPIO.LOW)
            time.sleep(SleepTimeL)
        else:
            GPIO.output(relay8, GPIO.HIGH)
            time.sleep(SleepTimeL)

GPIO.cleanup()
print("Good bye!")
time.sleep(SleepTimeL)
